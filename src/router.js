import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/pages/page-home/page-home.vue';

import Lightbox from '@/pages/page-lightbox/page-lightbox.vue';
import Carousels from '@/pages/page-carousel/page-carousels.vue';
import CarouselControlsIn from '@/pages/page-carousel/page-carousel-controls-in.vue';
import CarouselControlsOut from '@/pages/page-carousel/page-carousel-controls-out.vue';
import CarouselLightbox from '@/pages/page-carousel/page-carousel-lightbox.vue';
import CarouselLightboxComplex from '@/pages/page-carousel/page-carousel-lightbox-complex.vue';
import TransiRouter from '@/pages/page-transitions-router/page-transitions-router.vue';
import TransiFade from '@/pages/page-transitions-router/page-fade.vue';
import TransiFadeHeight from '@/pages/page-transitions-router/page-fade-height.vue';
import TransiFadeSlide from '@/pages/page-transitions-router/page-fade-slide.vue';
import TransiFadeSlideHeight from '@/pages/page-transitions-router/page-fade-slide-height.vue';
import TransiZoom from '@/pages/page-transitions-router/page-zoom.vue';
import TransiZoomHeight from '@/pages/page-transitions-router/page-zoom-height.vue';
import PageA from '@/pages/page-transitions-router/pages/pageA.vue';
import PageB from '@/pages/page-transitions-router/pages/pageB.vue';
import PageC from '@/pages/page-transitions-router/pages/pageC.vue';
import Svgs from '@/pages/page-svg/page-svg.vue';

import CssTricks from '@/pages/page-css-tricks/page-css-tricks.vue';
import PageBurgers from '@/pages/page-css-tricks/page-burgers.vue';
import PageUnderlineEffects from '@/pages/page-css-tricks/page-underline-effets.vue';
import PageCallToAction from '@/pages/page-css-tricks/page-call-to-action.vue';
import PageSplitting from '@/pages/page-css-tricks/page-splitting-effects.vue';


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Home,
    name: 'PageHome',
  },
  {
    path: '/carousels',
    component: Carousels,
    name: 'PageCarousels',
    children: [
      {
        path: '/carousels/carousel-controls-in',
        component: CarouselControlsIn,
        name: 'CarouselControlsIn',
      },
      {
        path: '/carousels/Carousel-controls-out',
        component: CarouselControlsOut,
        name: 'CarouselControlsOut',
      },
      {
        path: '/carousels/carousel-lightbox',
        component: CarouselLightbox,
        name: 'CarouselLightbox',
      },
      {
        path: '/carousels/carousel-lightbox-complex',
        component: CarouselLightboxComplex,
        name: 'CarouselLightboxComplex',
      },
    ],
  },
  {
    path: '/lightbox',
    component: Lightbox,
    name: 'PageLightbox',
  },
  {
    path: '/css-tricks',
    component: CssTricks,
    name: 'PageCssTricks',
    children: [
      {
        path: '/css-tricks/burgers',
        component: PageBurgers,
        name: 'PageBurgers',
      }, {
        path: '/css-tricks/underline-effects',
        component: PageUnderlineEffects,
        name: 'PageUnderlineEffects',
      },
      {
        path: '/css-tricks/call-to-action',
        component: PageCallToAction,
        name: 'PageCallToAction',
      },
      {
        path: '/css-tricks/splitting',
        component: PageSplitting,
        name: 'PageSplitting',
      }
    ],
  },
  {
    path: '/svg',
    component: Svgs,
    name: 'PageSvg',
  },
  {
    path: '/transirouter',
    component: TransiRouter,
    name: 'PageTransiRouter',
    children: [
      {
        path: '/transirouter/transi-fade',
        component: TransiFade,
        name: 'TransiFade',
        children: [
          {
            path: '/transirouter/transi-fade/subpage-a',
            component: PageA,
            name: 'PageAFade',
          },
          {
            path: '/transirouter/transi-fade/subpage-b',
            component: PageB,
            name: 'PageBFade',
          },
          {
            path: '/transirouter/transi-fade/subpage-c',
            component: PageC,
            name: 'PageCFade',
          },
        ],
      },
      {
        path: '/transirouter/transi-fade-height',
        component: TransiFadeHeight,
        name: 'TransiFadeHeight',
        children: [
          {
            path: '/transirouter/transi-fade-height/subpage-a',
            component: PageA,
            name: 'PageAFadeHeight',
          },
          {
            path: '/transirouter/transi-fade-height/subpage-b',
            component: PageB,
            name: 'PageBFadeHeight',
          },
          {
            path: '/transirouter/transi-fade-height/subpage-c',
            component: PageC,
            name: 'PageCFadeHeight',
          },
        ],
      },
      {
        path: '/transirouter/transi-fade-slide',
        component: TransiFadeSlide,
        name: 'TransiFadeSlide',
        children: [
          {
            path: '/transirouter/transi-fade-slide/subpage-a',
            component: PageA,
            name: 'PageAFadeSlide',
          },
          {
            path: '/transirouter/transi-fade-slide/subpage-b',
            component: PageB,
            name: 'PageBFadeSlide',
          },
          {
            path: '/transirouter/transi-fade-slide/subpage-c',
            component: PageC,
            name: 'PageCFadeSlide',
          },
        ],
      },
      {
        path: '/transirouter/transi-fade-slide-height',
        component: TransiFadeSlideHeight,
        name: 'TransiFadeSlideHeight',
        children: [
          {
            path: '/transirouter/transi-fade-slide-height/subpage-a',
            component: PageA,
            name: 'PageAFadeSlideHeight',
          },
          {
            path: '/transirouter/transi-fade-slide-height/subpage-b',
            component: PageB,
            name: 'PageBFadeSlideHeight',
          },
          {
            path: '/transirouter/transi-fade-slide-height/subpage-c',
            component: PageC,
            name: 'PageCFadeSlideHeight',
          },
        ],
      },
      {
        path: '/transirouter/transi-zoom',
        component: TransiZoom,
        name: 'TransiZoom',
        children: [
          {
            path: '/transirouter/transi-zoom/subpage-a',
            component: PageA,
            name: 'PageAZoom',
          },
          {
            path: '/transirouter/transi-zoom/subpage-b',
            component: PageB,
            name: 'PageBZoom',
          },
          {
            path: '/transirouter/transi-zoom/subpage-c',
            component: PageC,
            name: 'PageCZoom',
          },
        ],
      },
      {
        path: '/transirouter/transi-zoom-height',
        component: TransiZoomHeight,
        name: 'TransiZoomHeight',
        children: [
          {
            path: '/transirouter/transi-zoom-height/subpage-a',
            component: PageA,
            name: 'PageAZoomHeight',
          },
          {
            path: '/transirouter/transi-zoom-height/subpage-b',
            component: PageB,
            name: 'PageBZoomHeight',
          },
          {
            path: '/transirouter/transi-zoom-height/subpage-c',
            component: PageC,
            name: 'PageCZoomHeight',
          },
        ],
      },
    ],
  },
  { path: '*', redirect: '/' },
];

const router = new VueRouter({
  mode: 'history',
  routes: routes,
});

export default router;
